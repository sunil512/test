import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from './custom-validator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'test';
  firstFormGroup: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.firstFormGroup = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(16), Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.maxLength(16), Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      contact: ['', [Validators.required, Validators.pattern('[7-9]\\d{9}')]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  submitForm() {
    console.log(this.firstFormGroup.getRawValue());
  }

}
